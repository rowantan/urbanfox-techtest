# UrbanFox Tech Test
## URL Shortener
##### by Rowan Tan

Make sure you have Node JS, MongoDB installed on your system.

`brew install node`

`brew install mongodb`

Go to project root directory in your terminal and run:

`npm install mongodb`

`npm install express`

Start the database instance with:

`mongod`

Start the web server in new terminal, run:

`node app.js`

Go to `localhost:4555`
